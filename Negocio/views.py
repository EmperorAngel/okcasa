from django.shortcuts import render, render_to_response
from django.http import HttpResponse,JsonResponse
from django.shortcuts import redirect
from .models import Usuario,Solicitud,Horario,Equipo
from django.contrib.auth.models import User
from django.contrib.auth import authenticate,logout,login as auth_login
from django.contrib.auth.decorators import login_required,permission_required
from django.contrib.auth.models import Permission
import datetime
import calendar as cal_
from django.db.models import Q 
from django.views.decorators.csrf import csrf_exempt
# Create your views here.

def index(request):
    return render(request,'index.html', {})

def pago(request):
    return render(request,'pago.html', {})

def register_tecnico(request): 
    return render(request,'register-tecnico.html', {}) 

def listado_tecnico(request): 
    
    perm = Permission.objects.get(codename='is_tecnico')   
    tecnicos = User.objects.filter(Q(groups__permissions=perm) | Q(user_permissions=perm)) 
    return render(request,'listadotecnicos.html', {'tecnicos':tecnicos})

def tecnicoslist(request):
    perm = Permission.objects.get(codename='is_tecnico')   
    tecnicos = User.objects.filter(Q(groups__permissions=perm) | Q(user_permissions=perm)) 
    tec = [{'nombre':ob.first_name,'apellido':ob.last_name,'correo':ob.email} for ob in tecnicos]
    return JsonResponse({'tecnicos':tec})

def login(request):
    user = request.user
    if (user.is_anonymous):
        return render(request,'login.html',{})
    else:
        return render(request,'menu.html', {})
    

def register(request):
    return render(request,'register.html', {})

def recuperar(request):
    return render(request,'recuperar-contraseña.html',{})

def agendar(request):
    return render(request,'agendar.html',{})

def historial(request):
    return render(request,'historial.html',{})

def menu(request):
    user = request.user
    if user.has_perm('Negocio.is_cliente'):
        print("way1")
        return render(request,'menu.html', {})
    elif user.has_perm('Negocio.is_tecnico'):
        print("way2")
        return render(request,'tecnico.html', {})   
    elif user.has_perm('Negocio.is_administrador'):
        print("way3")
        return render(request,'admin.html', {})    
    elif user.is_anonymous:
        print("way4")
        return render(request,'login.html',{})
        

        
@login_required(login_url='login')
def admin(request):
    user = request.user
    return render(request,'admin.html', {})

@login_required(login_url='login')
def tecnico(request):
    user = request.user
    return render(request,'tecnico.html', {})


def crearUsuario(request):

    rut = request.POST.get('rut','')
    nombre= request.POST.get('nombre','')
    apellido = request.POST.get('apellido','')
    email = request.POST.get('email','')
    telefono = request.POST.get('telefono',0)
    contrasenia = request.POST.get('contrasenia','')
    

    u = User.objects.create_user(username=email,password=contrasenia,first_name=nombre,last_name=apellido,email=email)
    usuario = Usuario(credenciales=u,rut=rut,nombre=nombre,apellido=apellido,email=email,telefono=telefono)
    u.save()
    usuario.save()
    permission = Permission.objects.get(name='is cliente')
    u.user_permissions.add(permission)
    return redirect('login')

@login_required(login_url='login')
def crearTecnico(request):

    rut = request.POST.get('rut','')
    nombre= request.POST.get('nombre','')
    apellido = request.POST.get('apellido','')
    email = request.POST.get('email','')
    telefono = request.POST.get('telefono',0)
    contrasenia = request.POST.get('contrasenia','')
    

    u = User.objects.create_user(username=email,password=contrasenia,first_name=nombre,last_name=apellido,email=email)
    usuario = Usuario(credenciales=u,rut=rut,nombre=nombre,apellido=apellido,email=email,telefono=telefono)
    u.save()
    usuario.save()
    permission = Permission.objects.get(name='is tecnico')
    u.user_permissions.add(permission)
    return redirect('menu')

def login_iniciar(request):

    username = request.POST.get('user','')
    contrasenia = request.POST.get('password','')
    user = authenticate(request,username=username,password=contrasenia)
    print(user)
    if user is not None:
        print("way1")
        auth_login(request, user)
        request.session['usuario'] = user.first_name+" "+user.last_name
        return redirect("menu")
    else:
        print("way2")
        return redirect("login")

@login_required(login_url='login')
def cerrar_session(request):
    del request.session['usuario']
    logout(request)
    return redirect('index')

@csrf_exempt
def calendar(request):
    mes = request.POST.get('mes','')
    anno = request.POST.get('anno','')
    if(mes=='' or anno==''):
        return render(request,'calendar.html', {})
    else:
        mes=int(mes)+1 #por la diferencia entre javascript y python en cuanto a fechas se suma 1
        anno=int(anno)
        mesactual = datetime.datetime.now().month
        annoactual = datetime.datetime.now().year
        if(anno<annoactual or (anno==annoactual and mes<mesactual)):
            return JsonResponse({'max':0,'mes':0,'segmentos':0,'segdes':0})
        else:
            sol = Solicitud.objects.filter(anno=anno)
            sol = sol.filter(mes=mes)
            hor = Horario.objects.all()
            tothor = int(hor.count())
            eq = int(Equipo.objects.all().count())
            me = {}
            m = cal_.monthrange(int(anno),int(mes))[1]
            if(mesactual==mes and annoactual==anno):
                i=datetime.datetime.now().day
            else:
                i=1
            for d in range(i,m+1):
                day = {}
                ds = sol.filter(dia=d)
                for h in hor:
                    key = str(h.identificador)
                    day[key]= int(ds.filter(horario=h).count())
                me[d] = day
            segdes = {}
            for s in hor:
                segdes[s.identificador]={'inicio':s.inicio,'fin':s.fin}
            return JsonResponse({'max':eq,'mes':me,'segmentos':tothor,'segdes':segdes})

def crearsolicitud(request):
    anno = request.POST.get('anno','')
    mes = request.POST.get('mes','')
    dia = request.POST.get('dia','')
    hora = request.POST.get('hora','')
    success = False
    reason = ''
    user = request.user
    if(not user.is_anonymous):
        user = user.usuario
        if(anno!='' and mes!='' and  dia!='' and hora!=''):
            h = Horario.objects.get(identificador=hora)
            so = Solicitud.objects.filter(anno=anno).filter(mes=int(mes)+1).filter(dia=dia).filter(horario=h)
            eq = Equipo.objects.all()
            equipo = None
            for e in eq:
                if( not so.filter(equipo=e).exists()):
                    print(e.id)
                    equipo = e
            if(not equipo==None):
                sol = Solicitud(horario=h,usuario=user,dia=dia,mes=int(mes)+1,anno=anno,equipo=equipo)
                sol.save()
                success=True
            else:
                reason='no existen horas'
        else:
            reason='incomplete data'
    else:
        reason='user anonymous'
    return JsonResponse({'success':success,'reason':reason})