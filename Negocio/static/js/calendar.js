function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
  };
  
  var csrftoken = getCookie('csrftoken');



var year =new Date().getFullYear();
var month = new Date().getMonth();
var daysel = null;
var selectedhora = null;
var jsonresponse = null;

function nextmonth(){
    month=month+1;
    if(month==12){
        month=0;
        year=year+1;
    }
    loadcalendar(year,month);
}

function setnamemonth(month,year){
    var name = ''
    switch (month) {
        case 0:
            name='Enero';
            break;
        case 1:
            name='Febrero';
            break;
        case 2:
            name='Marzo';
            break;
        case 3:
            name='Abril';
            break;
        case 4:
            name='Mayo';
            break;
        case 5:
            name='Junio';
            break;
        case 6:
            name='Julio';
            break;
        case 7:
            name='Agosto';
            break;
        case 8:
            name='Septiembre';
            break;
        case 9:
            name='Octubre';
            break;
        case 10:
            name='Noviembre';
            break;
        case 11:
            name='Diciembre';
            break;
        default:
            break;
    }
    $('#mestitle').html(name+' '+year);
}

function loadcalendar(year,month){
    setnamemonth(month,year)
    var startday = (new Date(year,month,1).getDay())-1;
    var lastday = (new Date(year, month + 1, 0)).getDate();
    console.log(month);
    if(startday==-1){startday=6};
    console.log(startday);
    var k=startday;
    var f=1;
    for (var w = 0; w < 6; w++) {
        var week = '#week'+w;
        for (var d = 0; d < 7; d++) {
            $(week).children(day).empty();
            $(week).children(day).addClass('lock');
        }
    }

    for (var w = 0; w < 6; w++) {
        var week = '#week'+w;
        for (var d = 0; d < 7; d++) {
            var day = '.day'+d;
            if(f>lastday){break};
            if(d==k || startday!=k){
                $(week).children(day).removeClass('lock');
                $(week).children(day).html(f);
                f=f+1;
                k=k+1;
            }
        }
    }



    $.post('/calendar',{'mes':month,'anno':year},function(data){
        jsonresponse=data;
        for (var w = 0; w < 6; w++) {
            var week = '#week'+w;
            for (var d = 0; d < 7; d++) {
                var day = '.day'+d;
                var h=0;
                jsondia = data.mes[$(week).children(day).text()];
                if(jsondia!=null){
                    for(hor in jsondia){
                        h=h+jsondia[hor]
                    }
                    var horasdisponibles=data.max*data.segmentos - h
                    if(horasdisponibles<1){
                        var clase = 'nodis';
                    }
                    else{
                        var clase = 'dis';
                    }
                    f=$(week).children(day).text();
                    $(week).children(day).html((f).toString()+"<div class='"+clase+"'>"+(horasdisponibles).toString()+" horas disponibles</div>");
                }
                
                }
            }
            $('.dis').on('click',function(){
                var clicked = $(this).parent().clone().children().remove().end().text();
                daysel = clicked;
                segmentos = jsonresponse.mes[clicked];
                hor = jsonresponse.segdes;
                $('#segmentsdia').empty();
                for(key in segmentos){
                    console.log(segmentos[key]);
                    if(segmentos[key]<jsonresponse.max){
                        var clase = 'seg'
                    }
                    else{
                        var clase = 'full'
                    }
                    $('#segmentsdia').append("<div id='"+key+"' class='"+clase+"'>de "+hor[key]['inicio']+" a "+hor[key]['fin']+" </div>")
                }
                diaseg.open();
                $('.seg').on('click',function(){
                    $('.seg').removeClass('selected');
                    $(this).addClass('selected');
                    selectedhora = $(this).attr('id');
                })
            });
        })
}

loadcalendar(year,month);

$( document ).ready(function() {
    diaseg.open();
});

function agendar(){
    console.log(selectedhora+' '+daysel+' '+month+' '+year);
    console.log(jsonresponse);
    if(selectedhora!=null && daysel!=null && month!=null && year!=null){
        $.post('/crearsolicitud',{'csrfmiddlewaretoken': csrftoken,'anno':year,'mes':month,'dia':daysel,'hora':selectedhora},function(data){
            if(data.success){
                alert('solicitud creada');
                loadcalendar(year,month);
            }
            else{
                console.log(data);
            }
        })
    }
    else{
        
    }
}

